export default function (event) {
  const { target: { name, value } } = event;
  this.setState({ [name]: value });
}
