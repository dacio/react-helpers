# React Helpers

[![pipeline status](https://gitlab.com/dacio/react-helpers/badges/master/pipeline.svg)](https://gitlab.com/dacio/react-helpers/commits/master)
[![coverage report](https://gitlab.com/dacio/react-helpers/badges/master/coverage.svg)](https://gitlab.com/dacio/react-helpers/commits/master)


Various helper functions I use in my projects.

## Helpers

### handleInputChange

Stores the value of the input when put into an onChange handler.

Useful when creating controlled forms with value stored in state.


#### Example

```js
import React, { Component } from 'react';
import { handleInputChange } from '@dacio/react-helpers';

class ExampleComponent extends Component {
  state = {
    email: '';
  };

  handleInputChange = handleInputChange.bind(this);

  render() {
    const { email } = this.state;

    return (
      <input
        type="email"
        name="email"
        value={email}
        onChange={this.handleInputChange}
      />
    );
  }
}
```
