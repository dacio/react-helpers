// import jest from 'jest';
import handleInputChange from '../src/handleInputChange';

describe('handleInputChange', () => {
  it('it should call setState', () => {
    const name = 'email';
    const value = 'dacioromero@gmail.com';

    const event = {
      target: { name, value },
    };

    const setState = jest.fn();
    const fakeComponent = { setState };

    const boundHandler = handleInputChange.bind(fakeComponent);

    boundHandler(event);

    expect(setState.mock.calls.length).toBe(1);
    expect(setState.mock.calls[0][0]).toEqual({ [name]: value });
  });
});
